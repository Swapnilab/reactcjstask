import axios from "axios";
import { ALL_PRODUCTS, COLORS, FEATURED_PRODUCTS, MATERIAL } from "../constants";

export const RequestApi = axios.create({
  headers: {
    "Content-type": "application.json",
    Authorization: "Bearer Ex9yLyRU7wvyxfblpq5HAhfQqUP1vIyo",
  },
});

axios.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);


export const fetchProducts=async()=>{

return RequestApi.get(ALL_PRODUCTS)
}

export const fetchColors=async()=>{
 return RequestApi.get(COLORS)
}

export const fetchMaterails=async()=>{
 return RequestApi.get(MATERIAL)
}

export const fetchFeatured=async()=>{
 return RequestApi.get(FEATURED_PRODUCTS)
}