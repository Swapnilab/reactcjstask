
import { Route, Routes } from "react-router-dom"
import Navbar from "./components/Navbar";
import TopNavbar from "./components/TopNavbar";
import Products from "./components/Products";
import { useDispatch, useSelector } from "react-redux";
import { arrangeProduct, getColors, getFeatured, getMaterials, getProducts } from "./redux/productSlice";
import Sidbar from "./components/Sidbar";
//import AllProducts from "./components/AllProducts";
//import Featured from "./components/Featured";
import { lazy, Suspense, useEffect } from "react";
import { AllProductsFun } from "./utility"; 

const AllProducts=lazy(()=>import("./components/AllProducts"))
const Featured=lazy(()=>import("./components/Featured"))

function App() {
  const{products , colors , materials }= useSelector(state=>state.productsReducer)
  const dispatch = useDispatch();
  
  useEffect(()=>{
        dispatch(getColors ());
        dispatch(getMaterials ());      
        dispatch(getProducts());
        
    } ,[])
    
    useEffect(() => { dispatch(arrangeProduct(AllProductsFun(products ,materials , colors))); },
     [ products.length !== 0 && colors.length !== 0 && materials.length !== 0]);
  
  
  return (
    <div className="App">
      <TopNavbar />
      <Navbar />
      <div className="row">
        <div className="col-12">
          <Routes>
            <Route path="/" element={<Suspense fallback={<div style={{ position:"Fixed" ,left:"500px" , top:"400px" }}>Loading...</div>}><AllProducts/></Suspense> }/>
            <Route path="featured" element={<Suspense fallback={<div style={{ position:"Fixed",left:"500px" , top:"400px" }}>Loading...</div>}><Featured/></Suspense>}/>
            
          </Routes>
        </div>
      </div>
    </div>
  );
}

export default App;
