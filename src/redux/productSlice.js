import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  allData: [
    { id: 1, name: "All" },
    { id: 2, name: "Generic" },
    { id: 3, name: "Generic" },
    { id: 4, name: "Generic" },
    { id: 5, name: "Tasty" },
    { id: 6, name: "Tasty" },
    { id: 7, name: "Tasty" },
    { id: 8, name: "Gorgeous" },
    { id: 9, name: "Gorgeous" },
    { id: 10, name: "Gorgeous" },
    { id: 11, name: "Gorgeous" },
    { id: 12, name: "Gorgeous" },
  ],
  all: { id: 1, name: "All" },
  selectedColor: { id: 1, name: "All" },
  selectedMaterial: { id: 1, name: "All" },
  newMaterial: { id: 7, name: "silk" },
  products: [],
  colors: [],
  materials: [],
  featured: [],
  forFeatured: [],
  featuredProducts: [],
  arrangedProducts: [],
  arrangedFeatured: [],
  newColor: [],
  filteredProducts: [],
  filteredFeatured: [],
  isFilterPressed: false,
  cartCount: 0,
  cartProduct: [],
  selectedNav: "AllProducts",
  isStart: 0,
  featuredSelectedColor: { id: 1, name: "All" },
  featuredSelectedMaterial: { id: 1, name: "All" },
  arrangedFilteredFeatured: [],
  filteredProducts2: [],
  perfromFilterPro:[],
  perfromFilterFeat:[],
};
export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    setSelectedNav: (state, action) => {
      state.selectedNav = action.payload;
    },
    getProducts: (state, action) => {},
    getColors: () => {},
    getMaterials: () => {},
    getFeatured: () => {},
    setProducts: (state, action) => {
      state.products = action.payload;
      state.isStart = 1;
    },
    setColors: (state, action) => {
      const color = action.payload;
      color.unshift(state.all);
      state.colors = color;
    },
    setMaterials: (state, action) => {
      const material = action.payload;
      material.unshift(state.all);
      material.push(state.newMaterial);
      state.materials = material;
    },
    setFeatured: (state, action) => {
      state.featured = action.payload;
      state.forFeatured = state.featured.map((feat) => feat.productId);
    },
    arrangeProduct: (state, action) => {
     
      state.arrangedProducts = action.payload;
    },
    filterAtStart: (state, action) => {
      state.filteredProducts = state.arrangedProducts;
      state.perfromFilterPro=state.arrangedProducts;
    },
       filterFeaturedProducts: (state, action) => {  
      state.featuredProducts = state.arrangedProducts.filter((product) =>
        state.forFeatured.includes(product.id)
      );
      state.filteredProducts2=state.featuredProducts;
      state.perfromFilterFeat=state.featuredProducts
      state.selectedNav = "featured";
     
    },
    filterPro:(state , action)=>{
      const data=action.payload;
      state.filteredProducts =data
    },
    filterFeat:(state , action)=>{
      const data=action.payload;
      state.filteredProducts2=data;
    },
    setSelected: (state, action) => {
      const data = action.payload.data;
      action.payload.head === "Materials"
          ? (state.selectedMaterial = data)
           : (state.selectedMaterial = data);
    },
    setCartCount: (state, action) => {
      state.cartProduct.push(action.payload);
    },

  },
});

export const {
  filterPro ,
  filterFeat,
  arrangeProduct,
  getProducts,
  setProducts,
  getColors,
  getMaterials,
  getFeatured,
  setColors,
  setMaterials,
  setFeatured,
  filterAtStart,
  setSelected,
  setCartCount,
  filterFeaturedProducts,
  setSelectedNav,
} = productSlice.actions;
export default productSlice.reducer;
