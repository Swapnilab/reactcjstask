import {fork , put , call, takeEvery } from "redux-saga/effects"
import { fetchColors, fetchFeatured, fetchMaterails, fetchProducts } from "../api";

import { getColors, getFeatured, getProducts, getMaterials, setProducts, setColors, setMaterials, setFeatured } from "./productSlice";

function* onLoadGetColorsAsync(action){
try{
const response=yield call(fetchColors)

const {data}=response;
yield put(setColors([...data.colors]))
}
catch(error){

}
}

function* onLoadGetMaterialsAsync(action){
    try{
        const response=yield call(fetchMaterails);

        const{data}=response
        yield put(setMaterials([...data.material]))
    }catch(error){

    }
}

function* onLoadGetFeaturedAsync (action){
    try{
        const response =yield call(fetchFeatured)
        const {data}=response

yield put(setFeatured([...data.featured]))
    }catch(error){

    }
}


function* onLoadGetProductAsync(action){
    try{
    
        const response=yield call(fetchProducts)

        const {data}=response
        yield put(setProducts([...data.products]))
    }
    catch(error){

    }
}


function* watcherSaga(){


    yield takeEvery(getProducts.type , onLoadGetProductAsync);
    yield takeEvery(getColors.type , onLoadGetColorsAsync);
    yield takeEvery(getMaterials.type , onLoadGetMaterialsAsync);
    yield takeEvery(getFeatured.type , onLoadGetFeaturedAsync);
}


export const productSaga=[fork(watcherSaga)]