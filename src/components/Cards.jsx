import React, { forwardRef, useEffect, useId } from "react";
import "./Cards.css";
import { useDispatch, useSelector } from "react-redux";
import {
  filterAtStart,
  filterProducts,
  setCartCount,
} from "../redux/productSlice";

const Cards = ({ product, handleMouseLeave, handleMouseOver }) => {
  const id = useId();
  const id2 = useId();
  const { filteredProducts, selectedColor, selectedMaterial } = useSelector(
    (state) => state.productsReducer
  );
  const dispatch = useDispatch();

  return (
    <div className="col-4" id="card-div" key={product.id}>
      {" "}
      <div className="card" style={{ border: "none" }}>
        <img
          id={id}
          src={product.image}
          className="card-img-top imageBorder"
          style={{ borderRadius: "0px" }}
          alt="image"
          onMouseEnter={() =>
            handleMouseOver({
              id2: document.getElementById(id2),
            })
          }
        />

        <div
          id={id2}
          className="blurDiv"
          onMouseLeave={() =>
            handleMouseLeave({
              id2: document.getElementById(id2),
            })
          }
        >
          <div id="id4" onClick={() => dispatch(setCartCount(product))}>
            Add To Cart
          </div>
        </div>
        <div id="card_body">
          <span className="card_name">{product.name}</span>
          <div>
            <span className="card_color">{product.colorName}</span>
            <span className="card_material">{product.materialName}</span>
          </div>
          <span className="card-text" id="card_price">
            INR {product.price}
          </span>
        </div>
      </div>
    </div>
  );
};

export default Cards;
