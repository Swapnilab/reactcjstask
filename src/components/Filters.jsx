import React, { useId } from "react";
import HocForFilters from "./HocForFilters";
import "./Filters.css";
import { useDispatch, useSelector } from "react-redux";
import { setSelected } from "../redux/productSlice";

const Filters = ({
  color , material,
  heading,
  data,
  hello,
  handleClick,
  handleClick2,
  selectedNav,
}) => {
  const id = useId();

  const {
    selectedColor,
    selectedMaterial,
    featuredSelectedColor,
    featuredSelectedMaterial,
  } = useSelector((state) => state.productsReducer);
  const dispatch = useDispatch();

  
  const getClasses = (element) => {
    //if (selectedNav === "AllProducts") {
      if (heading === "Materials" && material.id === element.id)
        return "list weight";
      else if (heading === "Colors" && color.id === element.id)
        return "list weight";
      else if (heading === "Tags" && element.id === 1) return "list weight";
      else return "list";
    //} 
    // else {
    //   if (heading === "Materials" && featuredSelectedMaterial.id === element.id)
    //     return "list weight";
    //   else if (heading === "Colors" && featuredSelectedColor.id === element.id)
    //     return "list weight";
    //   else if (heading === "Tags" && element.id === 1) return "list weight";
    //   else return "list";
    // }
  };

  return (
    <div>
      <span id="heading">{heading}</span>
      <div id="scroll-div">
        {" "}
        <ul id="no-bulltes">
          {data.map((listElements, i) => (
            <li
              key={listElements.id}
              id={id}
              className={getClasses(listElements)}
              onClick={() => {
                handleClick(listElements);
              }}
            >
              <a style={{ textTransform: "capitalize" }}>{listElements.name}</a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default HocForFilters(Filters);
