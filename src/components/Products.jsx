import "./Allproducts.css";
import React from "react";
import Filters from "./Filters";
import Cards from "./Cards";
import ProductsHoc from "./ProductsHoc";
import Sidbar from "./Sidbar";

const Products = (props) => {
  
  return (
    <div className="main-div">
      <div className="row">
        <div className="col-2" id="menu">
         <Sidbar setFilter={(data , head)=>props.setFilter(data , head) } {...props}/>
        </div>
        <div className="col-10">
          {" "}
          <div className="row" style={{ margin: "10px 0px" }}>
            {props.filteredProduct.length !== 0 &&
              props.filteredProduct?.map((product, index) => (
                <Cards
                  key={product.id}
                  product={product}
                  handleMouseLeave={(id) => props.handleMouseLeave(id)}
                  handleMouseOver={(id) => props.handleMouseOver(id)}
                />
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductsHoc(Products);
