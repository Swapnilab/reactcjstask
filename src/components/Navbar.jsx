import React from "react";
import "./Navbar.css";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  filterFeaturedProducts,
  setSelected,
  setSelectedNav,
} from "../redux/productSlice";

const Navbar = () => {
  const { cartProduct, selectedNav } = useSelector(
    (state) => state.productsReducer
  );
  const dispatch = useDispatch();
  const getClasses2 = () => {
    return selectedNav === "AllProducts"
      ? "Navbar-link navBold"
      : "Navbar-link";
  };
  const getClasses = () => {
    return selectedNav !== "AllProducts"
      ? "Navbar-link navBold"
      : "Navbar-link";
  };
  return (
    <div>
      {" "}
      <div className="navbar-div">
        <nav className="navbar navbar-expand-lg bg-body-tertiary">
          <div className="container-fluid">
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav me-auto mb-2 mb-lg-0" id="ull">
                <li className="nav-item">
                  <Link className="nav-link active " aria-current="page" to="/">
                    <div
                      style={{ textDecoration: "none" }}
                      className={getClasses2()}
                      onClick={() => dispatch(setSelectedNav("AllProducts"))}
                    >
                      All Products
                    </div>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link active" to="/featured">
                    <div
                      style={{ textDecoration: "none" }}
                      className={getClasses()}
                      onClick={()=>dispatch(setSelectedNav("featured"))}
                      // onClick={() => dispatch(filterFeaturedProducts())}
                    >
                      Featured Products
                    </div>
                  </Link>
                </li>
              </ul>
              <img
                id="img1"
                src={process.env.PUBLIC_URL + "/shop.png"}
                alt="CART"
              />
              <span className="cart-product">{cartProduct?.length} </span>
            </div>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
