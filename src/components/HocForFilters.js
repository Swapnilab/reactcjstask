import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setSelected } from "../redux/productSlice";

const HocForFilters = (OldFilter) => {
  const NewFilter = (props) => {
    const dispatch = useDispatch();
    const Allproducts = useSelector((state) => state.productsReducer);
    const { selectedNav } = Allproducts;
    const handleClick = (data) => {
    
      if (props.heading !== "Tags") {
        
        //dispatch(setSelected({ data, head: props.heading }));
        props.setFilter(data , props.heading)
      }
    };
    

    return (
      <OldFilter
        {...props}
        name={"name"}
        handleClick={(data) => handleClick(data)}
        selectedNav={selectedNav}
      />
    );
  };

  return NewFilter;
};

export default HocForFilters;
