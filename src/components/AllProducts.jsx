import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {  filterAtStart, filterPro } from '../redux/productSlice';
import Products from './Products'

const AllProducts = () => {
    const dispatch=useDispatch();
    const{filteredProducts ,arrangedProducts , perfromFilterPro }= useSelector(state=>state.productsReducer)

    const [color , setColor]=useState({id:1 , name:"All"})
    const [material , setMaterial]=useState({id:1 , name:"All"})

    const setFilter=(data , head)=>{
     const data1=data; const head1=head;
     if(head==="Colors"){ setColor(data1)}
     else{setMaterial(data1)}
    }

    const setProductsToShow=(data)=>{
      console.log(" data after filter" , data);
      dispatch(filterPro(data));
    }
    
    useEffect(()=>{dispatch(filterAtStart()); } ,[arrangedProducts.length!==0])

    
  return (
    <Products
    color={color}
    material={material}
    setFilter={(data , head)=>setFilter(data , head)} 
    setProductsToShow={(data)=>setProductsToShow(data)}
    productsToDisplay={filteredProducts}
    performFilter={perfromFilterPro}
    />
    
  )
}

export default AllProducts
