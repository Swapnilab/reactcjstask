import React from "react";
import { useSelector } from "react-redux";
import "./Allproducts.css";
import Filters from "./Filters";

const Sidbar = (props) => {
  const Allproducts = useSelector((state) => state.productsReducer);
  const { allData, materials, colors  } = Allproducts;
  return (

    // <div className="main-div">
    //   <div id="menu">
    //  <div>
      <div>
        <Filters {...props} data={allData} heading="Tags" hello="hello" />
        <Filters {...props} data={materials} heading="Materials" hello="hello" />
        <Filters {...props} data={colors} heading="Colors" hello="hello" />
      </div>
   // </div>
  );
};

export default Sidbar;
