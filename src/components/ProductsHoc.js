import React, { useState, useEffect, useTransition } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  
} from "../redux/productSlice";

const ProductsHoc = (Product) => {
  const NewProduct = (props) => {
    const [isPending, startTransition] = useTransition();
    const Allproducts = useSelector((state) => state.productsReducer);
    const dispatch = useDispatch();
    const {allData,colors,materials} = Allproducts;
    const [all, setAll] = useState(allData);
    const [ filtered , setFiltered] =useState([])
    const [isProps , setIsProps]=useState(false)    
    const handleMouseOver = (obj) => {
      obj.id2.style.visibility = "visible";
    };

    const handleMouseLeave = (obj) => {
      obj.id2.style.visibility = "hidden";
    };

    const filterData= () => {
      console.log("filter data called")
      const  productsFiltered = props.performFilter.filter(
        (product, index) => {
          if (
            props.color.name !== "All" &&
            props.material.name === "All"
          ) {
            return product.colorName === props.color.name;
          } else if (
            props.material.name !== "All" &&
            props.color.name === "All"
          ) {
            return product.materialName === props.material.name;
          } else if (
            props.color.name !== "All" ||
            props.material.name !== "All"
          ) {
            return (
              product.materialName === props.material.name &&
              product.colorName === props.color.name
            );
          } else if (
            props.color.name === "All" &&
            props.material.name === "All"
          ) {
            return product.id;
          }
        }
      );


      console.log("at the end filter products" , productsFiltered)
      // setFiltered(productsFiltered);
      // setIsProps(true)
      props.setProductsToShow(productsFiltered)
    };

   
    useEffect( ()=>{ filterData() } ,[] )

    useEffect(()=>{ filterData(); } ,[props.color , props.material])

    console.log("final filtered at bottom " , filtered)
    return (
      <Product
      {...props}
        all={all}
        materials={materials}
        colors={colors}
        filteredProduct={props.productsToDisplay}
        handleMouseOver={(id) => handleMouseOver(id)}
        handleMouseLeave={(id) => handleMouseLeave(id)}
      />
    );
  };

  return NewProduct;
};

export default ProductsHoc;
