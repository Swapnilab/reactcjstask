import React from "react";
const HocForCards = (OldCard, id) => {
  const NewCard = (props) => {
    const handleMouseLeave = () => {
      id.style.opacity = 1;
    };
    const handleMouseOver = (id) => {
      id.style.opacity = 0.5;
    };

    return (
      <OldCard
        {...props}
        handleMouseLeave={() => handleMouseLeave()}
        handleMouseOver={() => handleMouseOver()}
      />
    );
  };

  return NewCard;
};

export default HocForCards;
