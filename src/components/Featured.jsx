import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { filterFeat, filterFeaturedProducts, getFeatured } from '../redux/productSlice';
import Products from './Products'

const Featured = () => {

    const{products , colors , materials , featured, arrangedProducts , perfromFilterFeat , filteredProducts2}= useSelector(state=>state.productsReducer)
    const dispatch=useDispatch()

    const [colorFeat , setColorFeat]=useState({id:1 , name:"All"})
    const [materialFeat , setMaterialFeat]=useState({id:1 , name:"All"})
    
    useEffect(()=>{
        dispatch(getFeatured());
    } ,[products.length!==0 && colors.length!==0 && materials.length!==0])

    useEffect(()=>{ dispatch(filterFeaturedProducts()); } ,[featured.length!==0 && arrangedProducts.length!==0])
   
    const setFilter=(data , head)=>{
      const data1=data; const head1=head;
      if(head==="Colors"){
       setColorFeat(data1)
      }
      else{
       setMaterialFeat(data1)
     }
 
     }

     const setProductsToShow=(data)=>{
      console.log(" data after filter" , data);
      dispatch(filterFeat(data));

    }
   
  return (
   <Products 
    color={colorFeat}
    material={materialFeat}
    setFilter={(data , head)=>setFilter(data , head)} 
    setProductsToShow={(data)=>setProductsToShow(data)}
   productsToDisplay={filteredProducts2}
   performFilter={perfromFilterFeat}
   />
  )
}

export default Featured
