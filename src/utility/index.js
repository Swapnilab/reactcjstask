 export const AllProductsFun=(products , materials , colors)=>{
    const newArr = products.map((product) => {
        const newMat = materials.filter((material, i) => material.id === product.materialId + 1);
        const newcol = colors.filter((color, i) => color.id === product.colorId + 1);

        return {
          ...product,
          materialName: newMat[0].name,
          colorName: newcol[0].name,
        };
      });
return newArr
}